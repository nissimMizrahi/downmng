extern crate reqwest;
use reqwest::header;

use std::fs;
use std::fs::File;
use std::io::prelude::*;

extern crate rayon;
use rayon::prelude::*;
use rayon::ThreadPool;

mod DiskBufferMod;
use DiskBufferMod::DiskBuffer;

use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

use std::cmp;

fn getPages(num: u32, splits: u32) -> Vec<(u32, u32)> {
    let mut ret = Vec::new();

    let mut i = 0;
    while i < num {
        let start = i;
        i += num / splits;
        ret.push((start, cmp::min(i, num)));
    }

    return ret;
}

fn getPageLen(url: &str) -> u32 {
    let client = reqwest::blocking::Client::builder().build().unwrap();
    let resp = client.head(url).send().unwrap();

    return resp.headers()["content-length"]
        .to_str()
        .unwrap()
        .to_string()
        .parse::<u32>()
        .unwrap();
}

fn getPageSlice(url: &str, start: u32, end: u32) -> Vec<u8> {
    let mut headers = header::HeaderMap::new();
    headers.insert(
        header::RANGE,
        header::HeaderValue::from_str(&format!("bytes={}-{}", start, end)).unwrap(),
    );

    let client = reqwest::blocking::Client::builder()
        .default_headers(headers)
        .build()
        .unwrap();

    println!("{} => {} query", start, end);
    let mut resp = client.get(url).send().unwrap();

    let mut vec = Vec::with_capacity((end - start) as usize);
    resp.read_to_end(&mut vec).expect("Unable to read data");
    println!("{} => {} has response", start, end);

    return vec;
}

fn main() {
    //let mut headers = header::Headers::new();
    //headers.set(header::Authorization("secret".to_string()));
    //.default_headers()

    let threads = 50;

    rayon::ThreadPoolBuilder::new()
        .num_threads(threads)
        .build()
        .unwrap()
        .install(|| {
            let url = "https://download3.vmware.com/software/wkst/file/VMware-Workstation-Full-15.5.2-15785246.x86_64.bundle";

            let dir = "./downs";
            let mut len = getPageLen(url);

            let results = getPages(len, threads as u32)
                .par_iter()
                .map(|(start, end)| {
                    let new_end = if *end != len { *end - 1 } else { *end };
                    //let mut file = File::create(&path).unwrap();
                    return getPageSlice(url, *start, new_end);
                })
                .collect::<Vec<Vec<u8>>>();

            println!("has results");
            let mut out = File::create("./blabla2.jpg").unwrap();
            for vec in results {
                out.write(&vec);
            }
            println!("wrote results");
        });
    //let client = reqwest::blocking::Client::builder().build().unwrap();
    //let resp = client.head("https://docs.rs/reqwest/0.7.3/reqwest/header/struct.ContentType.html").send().unwrap();

    //println!("{:#?}", results);
}
